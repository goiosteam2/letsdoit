//
//  AppDelegate.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 02.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import FBSDKLoginKit
import GoogleMaps
import GooglePlaces
import Parse
import ParseFacebookUtilsV4
import ZendeskSDK
import Branch
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        GMSServices.provideAPIKey(kGMSApiKey)
        GMSPlacesClient.provideAPIKey(kGMSApiKey)
        
        Parse.enableLocalDatastore()
        let parseConfiguration = ParseClientConfiguration(block: { (ParseMutableClientConfiguration) -> Void in
            ParseMutableClientConfiguration.applicationId = kParseApplicationId
            ParseMutableClientConfiguration.clientKey = kParseClientKey
            ParseMutableClientConfiguration.server = kParseServer
        })
        Parse.initialize(with: parseConfiguration)
    
        PFFacebookUtils.initializeFacebook(applicationLaunchOptions: launchOptions)
        
        User.fillCurrent()
        
        ZDKConfig.instance().initialize(withAppId: 
                kZendeskAppID,
                zendeskUrl: "https://letsdoitukraine.zendesk.com",
                clientId: "mobile_sdk_client_1f04c6e3487bbed0c737")
        let anonymousIdentity = ZDKAnonymousIdentity()
        ZDKConfig.instance().userIdentity = anonymousIdentity
    
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, automaticallyDisplayDeepLinkController: true, deepLinkHandler: { params, error in
            if error == nil {
                print("params: %@", params.description)
            }
        })
        IQKeyboardManager.sharedManager().enable = true
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        /*
        Crashlytics.sharedInstance().setUserEmail("user@fabric.io")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
         */
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        Branch.getInstance().handleDeepLink(url)
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation]) || true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        Branch.getInstance().continue(userActivity)
        return true
    }

}

