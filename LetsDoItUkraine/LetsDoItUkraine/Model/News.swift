//
//  News.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 04.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import Foundation
import UIKit

struct PieceOfNews {
    
    let id: String!
    let title: String!
    let body: String!
    let date: Date!
    let link: URL?
    let picture: URL?
    
}

class News {
    
    static let shared = News()
    
    var news: Array<PieceOfNews>
    
    private init() {
      self.news = []
    }
}
