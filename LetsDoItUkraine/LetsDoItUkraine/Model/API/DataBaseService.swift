//
//  DataBaseService.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 03.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import Parse
import SDWebImage
import PKHUD

protocol DataServiceProtocol {
    func recyclePointsWithinGeoBox(fromSouthwest southwest:CLLocationCoordinate2D, toNortheast northeast:CLLocationCoordinate2D, filter: Array<Int>, callback:(()->Void)?)
    func cleaningsWithinGeoBox(fromSouthwest southwest:CLLocationCoordinate2D, toNortheast northeast:CLLocationCoordinate2D,  callback:(()->Void)?)
    func createCleaning(_ cleaning:Dictionary<String, Any?>, callback:((Bool)->Void)?)
}

class DataBaseService: DataServiceProtocol {

    static let shared = DataBaseService()

    //MARK: Map points 
    
    func recyclePointsWithinGeoBox(fromSouthwest SW:CLLocationCoordinate2D, toNortheast NE:CLLocationCoordinate2D, filter: Array<Int>, callback:(()->Void)?) {
        
        let SWPoint = PFGeoPoint(latitude: SW.latitude, longitude: SW.longitude)
        let NEPoint = PFGeoPoint(latitude: NE.latitude, longitude: NE.longitude)
        
        let query = PFQuery(className:"RecyclePoint")
        query.whereKey("location", withinGeoBoxFromSouthwest: SWPoint, toNortheast: NEPoint)
        if !filter.isEmpty {
            query.whereKey("categories", containedIn: filter)
        }
        query.limit = 1000
    
        query.findObjectsInBackground(block: { (items, error) in
            
            guard let placesObjects = items else {
                    print("Error occured while finding recycle points on map!")
                    return
            }
            
            var recyclePoints:Array<RecyclePoint> = []
            
            for item in placesObjects {
                
                guard let locationPF = item["location"] as? PFGeoPoint else {
                    continue
                }
                let location = CLLocationCoordinate2D(latitude: locationPF.latitude,
                                                      longitude: locationPF.longitude)
                let id = item.objectId
                let title = item["title"] as? String
                let weight = item["weight"] as? String
                let phone = item["phone"] as? String
                let address = item["address"] as? String
                let schedule = item["schedule"] as? String
                let summary = item["summary"] as? String
                
                var websiteUrl:URL?
                var logo:URL?
                var photo:URL?
                var categories:Array<RubbishType>?
                
                if let website = item["website"] as? String {
                    websiteUrl = URL(string: website)
                }
                if let categoriesInt = item["categories"] as? Array<Int> {
                    categories = categoriesInt.map({ (intType) -> RubbishType in
                        return RubbishType(rawValue: intType)!
                    })
                }
                if let logoPF = item["logo"] as? PFFile,
                    let logoUrl = logoPF.url {
                    logo  = URL(string: logoUrl)
                }
                if let photoPF = item["photo"] as? PFFile,
                    let photoUrl = photoPF.url {
                    photo = URL(string: photoUrl)
                }
                
                let recyclePoint = RecyclePoint(id: id, categories: categories, location: location, title: title, weight: weight, phone: phone, website: websiteUrl, logo: logo, photo: photo, address: address, schedule: schedule, summary: summary)
                
                recyclePoints.append(recyclePoint)
            }
            RecyclePoints.shared.current = recyclePoints
            callback?()
        })
        
    }
    
    func cleaningsWithinGeoBox(fromSouthwest SW:CLLocationCoordinate2D, toNortheast NE:CLLocationCoordinate2D,  callback:(()->Void)?) {
        
        let SWPoint = PFGeoPoint(latitude: SW.latitude, longitude: SW.longitude)
        let NEPoint = PFGeoPoint(latitude: NE.latitude, longitude: NE.longitude)
        
        let query = PFQuery(className:"Cleaning")
        query.whereKey("location", withinGeoBoxFromSouthwest: SWPoint, toNortheast: NEPoint)
        query.limit = 1000
        
        query.findObjectsInBackground(block: { (items, error) in
            
            guard let placesObjects = items else {
                    HUD.flash(.error)
                    print("Error occured while finding cleanings on map!")
                    return
            }
            var cleanings:Array<Cleaning> = []
            for item in placesObjects {
                guard let cleaning = self.createCleaningFrom(item) else {
                    continue
                }
                cleanings.append(cleaning)
            }
            Cleanings.shared.current = cleanings
            callback?()
        })
        
    }
    
    func createCleaningFrom(_ item:PFObject) -> Cleaning? {
        guard let locationPF = item["location"] as? PFGeoPoint else {
            return nil
        }
        
        func addPictureUrlFromPFObject(_ object:PFObject, to array: inout Array<URL?>, named name:String) {
            if let pict = object[name] as? PFFile,
                let url = pict.url {
                array.append(URL(string:url))
            }
        }
        
        let location = CLLocationCoordinate2D(latitude: locationPF.latitude,
                                              longitude: locationPF.longitude)
        
        let id = item.objectId
        let title = item["title"] as? String
        var coordinator:User? = nil
        if let pfCoordinator = item["coordinator"] as? PFUser {
            coordinator = User.createWith(pfCoordinator)
        }
        let date = item["datetime"] as? Date
        let address = item["address"] as? String
        let about = item["about"] as? String
        let query = item.relation(forKey: "volunteers").query()
        var countError:NSError? = nil
        let numberOfPeople = query.countObjects(&countError)
        
        var pictures:Array<URL?> = []
        addPictureUrlFromPFObject(item, to: &pictures, named: "picture1")
        addPictureUrlFromPFObject(item, to: &pictures, named: "picture2")
        addPictureUrlFromPFObject(item, to: &pictures, named: "picture3")
        
        let cleaning = Cleaning(id: id, title: title, location: location, datetime: date, address: address, pictures: pictures, about: about, coordinator: coordinator, numberOfPeople:numberOfPeople)
        return cleaning
    }
    
    
    //MARK: - Cleanings
    
    func createCleaning(_ dict:Dictionary<String, Any?>, callback:((Bool)->Void)?) {
        
        var dictPFObject = [String : Any]()
        
        func findFieldAddToDict(_ key:String) {
            if let value =  dict[key] {
                dictPFObject[key] = value
            }
        }
        let location = dict["location"] as! CLLocationCoordinate2D
        dictPFObject["location"] = PFGeoPoint(latitude: location.latitude, longitude: location.longitude)
        dictPFObject["coordinator"] = PFUser.current()
        
        findFieldAddToDict("title")
        findFieldAddToDict("about")
        findFieldAddToDict("address")
        findFieldAddToDict("datetime")
        
        let cleaningPF = PFObject(className: "Cleaning", dictionary: dictPFObject)
        
        if let pict1 = dict["picture1"] as? UIImage?,
            let _ = pict1,
            let data = UIImageJPEGRepresentation(pict1!, 0.5) {
            let file = PFFile(data: data, contentType: "image/jpeg")
            cleaningPF.setObject(file, forKey: "picture1")
        }

        if let pict2 = dict["picture2"] as? UIImage?,
            let _ = pict2,
            let data = UIImageJPEGRepresentation(pict2!, 0.5) {
            let file = PFFile(data: data, contentType: "image/jpeg")
            cleaningPF.setObject(file, forKey: "picture2")
        }
        
        if let pict3 = dict["picture3"] as? UIImage?,
            let _ = pict3,
            let data = UIImageJPEGRepresentation(pict3!, 0.5) {
            let file = PFFile(data: data, contentType: "image/jpeg")
            cleaningPF.setObject(file, forKey: "picture3")
        }
        
        cleaningPF.saveInBackground { (result, error) in
            if error == nil {
                callback?(true)
            } else {
                callback?(false)
                print(error!.localizedDescription)
            }
        }
    }
    
    func addCurrentUser(to cleaning:Cleaning, callback: ((Bool)->Void)?) {
        if let user = PFUser.current() {
            addUser(user, to: cleaning, callback: callback)
        }
    }
    
    func addUser(_ user:PFUser, to cleaning:Cleaning, callback:((Bool)->Void)?) {
        let query = PFQuery(className: "Cleaning")
        query.getObjectInBackground(withId: cleaning.id!) { (result, error) in
            guard let cleaningPF = result else {
                HUD.flash(.labeledError(title: "Ошибка", subtitle: "Проверьте интернет подключение"))
                callback?(false)
                return
            }
            let relation = cleaningPF.relation(forKey: "volunteers")
            relation.add(user)
            cleaningPF.saveInBackground(block: { (success, error) in
                if !success {
                    HUD.flash(.labeledError(title: "Ошибка", subtitle: "Проверьте интернет подключение"))
                    callback?(false)
                } else {
                    callback?(true)
                }
            })
        }
    }
    
    func currentCleaning(callback:((Cleaning?, Bool)->())?) {
        guard let pfUser = PFUser.current() else {
            return
        }
        let query = PFQuery(className: "Cleaning")
        query.whereKey("volunteers", equalTo: pfUser).addDescendingOrder("datetime")
        query.findObjectsInBackground { (foundObjects, error) in
            if let items = foundObjects,
                let item = items.first {
                if let cleaning = self.createCleaningFrom(item) {
                    User.current?.currentCleaning = cleaning
                    callback?(cleaning, false)
                } else {
                    callback?(nil, true)
                }
            }
        }
    }
    
    func cleaningHistory(callback:(([Cleaning]?, Bool)->())?) {
        guard let pfUser = PFUser.current() else {
            return
        }
        let query = PFQuery(className: "Cleaning")
        query.whereKey("volunteers", equalTo: pfUser).addDescendingOrder("datetime")
        query.findObjectsInBackground { (items, error) in
            guard let foundObjects = items else {
//                HUD.flash(.error)
                print("Error occured while retrieving history!")
                return
            }
            var cleanings = [Cleaning]()
            for item in foundObjects {
                if let cleaning = self.createCleaningFrom(item) {
                    cleanings.append(cleaning)
                }
            }
            User.current?.cleaningsHistory = cleanings
            callback?(cleanings, false)
        }
    }
    
    func coordinatorHistory(callback:((Int, Bool)->())?) {
        guard let pfUser = PFUser.current() else {
            return
        }
        coordinatorHistory(pfUser, callback: callback)
    }
    
    func coordinatorHistory(_ user:PFUser, callback:((Int, Bool)->())?) {
        let query = PFQuery(className: "Cleaning")
        query.whereKey("coordinator", equalTo: user)
        query.countObjectsInBackground { (result, error) in
            if error == nil {
                callback?(Int(result), false)
            } else {
                callback?(0, true)
            }
        }
    }
    
    func coordinatorHistory(_ userId:String, callback:((Int, Bool)->())?) {
        let query = PFQuery(className: "User")
        query.getObjectInBackground(withId: userId) { (pfUser, error) in
            if let user = pfUser as? PFUser {
                self.coordinatorHistory(user, callback: callback)
            } else {
                callback?(0,true)
            }
        }
    }
    
    func volunteerHistory(callback:((Int, Bool)->())?) {
        guard let pfUser = PFUser.current() else {
            return
        }
        volunteerHistory(pfUser, callback: callback)
    }
    
    func volunteerHistory(_ user:PFUser, callback:((Int, Bool)->())?) {
        let query = PFQuery(className: "Cleaning")
        query.whereKey("volunteer", equalTo: user)
        query.countObjectsInBackground { (result, error) in
            if error == nil {
                callback?(Int(result), false)
            } else {
                callback?(0, true)
            }
        }
    }
    
    func volunteerHistory(_ userId:String, callback:((Int, Bool)->())?) {
        let query = PFQuery(className: "User")
        query.getObjectInBackground(withId: userId) { (pfUser, error) in
            if let user = pfUser as? PFUser {
                self.volunteerHistory(user, callback: callback)
            } else {
              callback?(0,true)
            }
        }
    }
    
    //MARK: - News
    
    func getNews(callback:(()->Void)?) {
        
        let query = PFQuery(className: "News")
        
        query.addDescendingOrder("updatedAt")
        query.skip = News.shared.news.count
        
        query.findObjectsInBackground { (items, error) in
            
            guard let newsObjects = items else {
                print("No more news to show!")
                return
            }
            
            var currentNews = News.shared.news
            
            for item in newsObjects {
                let id = item.objectId
                let date = item.createdAt
                guard let title = item["title"] as? String else {
                    continue
                }
                guard let body = item["body"] as? String else {
                    continue
                }
                var url:URL?
                var picture:URL?
                
                if let link = item["link"] as? String {
                    url = URL(string: link)
                }
                //TODO: load pictures in background
                if let picturePF = item["image"] as? PFFile,
                    let pictureUrl = picturePF.url {
                    picture  = URL(string:pictureUrl)
                }
                
                let pieceOfNews = PieceOfNews(id: id, title: title, body: body, date: date, link: url, picture: picture)
                
                currentNews.append(pieceOfNews)
            }
            
            News.shared.news = currentNews
            callback?()
        }
    }
}
