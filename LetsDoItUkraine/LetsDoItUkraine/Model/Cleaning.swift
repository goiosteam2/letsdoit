//
//  Cleaning.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 04.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import  UIKit
import CoreLocation

struct Cleaning: MapPointProtocol, Equatable {
    
    let id: String?
    var title: String?
    var location: CLLocationCoordinate2D?
    var datetime: Date?
    var address: String?
    var pictures: Array<URL?>?
    var about: String?
    var coordinator:User?
    var numberOfPeople:Int = 0
    
    //MARK: - MapPointProtocol
    
    var icon: URL? {
        return pictures?.first!
    }
    var heading: String? {
        return title
    }
    var subHeading: String? {
        return address
    }
    var designation: String? {
        if let name = coordinator?.fullName {
            return "Координатор: " + name
        } else {
            return "Координатор: отсутствует"
        }
    }
    var subtitle: String? {
        if numberOfPeople != 0 {
            return "Идут на уборку: " + numberOfPeople.description
        } else {
            return "Будьте первым, кто пойдет!"
        }
    }
    var position: CLLocationCoordinate2D? {
        return location
    }
    
    //MARK: - Equatable
    public static func ==(lhs: Cleaning, rhs: Cleaning) -> Bool {
        return lhs.id == rhs.id
    }
    
    //MARK: - Actions
    
    func addCurrentUser(callback:((Bool)->Void)?) {
        DataBaseService.shared.addCurrentUser(to: self, callback: callback)
    }
    
}

class Cleanings: MapPoints {
    
    static let shared = Cleanings()
    
    var current:Array<Cleaning> = []
    
    private init() {
    }
    
    //MARK: MapPoints
    
    typealias PointType = Cleaning
    
    func clearCurrent() {
        DispatchQueue.main.async(flags: .barrier) {
            self.current.removeAll()
        }
    }
}


protocol MapPointProtocol {
    var icon: URL? {get}
    var heading: String? {get}
    var subHeading: String? {get}
    var designation: String? {get}
    var subtitle: String? {get}
    var position: CLLocationCoordinate2D? {get}
}

protocol MapPoints {
    associatedtype PointType
    var current:Array<PointType> {get set}
    func clearCurrent()
}
