//
//  User.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 04.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import CoreLocation
import ParseFacebookUtilsV4

class User {
    
    static var current:User? = nil
    private init() {
        self.id = nil
    }
    
    init?(_ dict:Dictionary<String,Any?>) {
        guard let id = dict["id"] as? String,
            id != "",
            let firstName = dict["firstName"] as? String,
            firstName != "" else {
            return nil
        }
        
        self.id = id
        self.firstName = firstName
        self.lastName = dict["lastName"] as? String
        self.phone = dict["phone"] as? String
        self.email = dict["email"] as? String
        self.pictureUrl = dict["pictureUrl"] as? URL
        self.location = dict["location"] as? String
        self.currentCleaning = dict["currentCleaning"] as? Cleaning
    }
    
    let id: String!
    var firstName: String!
    var lastName: String?
    var phone: String?
    var email: String!
    var pictureUrl: URL?
    var location: String?
    var currentCleaning: Cleaning?
    var cleaningsHistory = [Cleaning]()
    
    var fullName: String {
        var name = ""
        if let firstName = firstName {
            name = name + firstName
        }
        if let lastName = lastName {
            name = name + " " + lastName
        }
        return name
    }
    
}

//MARK: Authentication

extension User {
    
    class func signIn(callback:(()->Void)?) {
        
        PFFacebookUtils.logInInBackground(withReadPermissions: ["public_profile","email"], block: { (result, error) in
            
            guard let user = result as PFUser? else {
                return
            }

            if(FBSDKAccessToken.current() != nil) {
                
            }
            print(user)
            print("Current user token=\(FBSDKAccessToken.current().tokenString)")
            print("Current user id \(FBSDKAccessToken.current().userID)")
            
            let userDetails = FBSDKGraphRequest(graphPath: "me",
                                                parameters: ["fields": "id, first_name, last_name, email"])
            
            let _ = userDetails?.start(completionHandler: { (connection, result, error) in
                
                if let error = error {
                    print("\(error.localizedDescription)")
                    return
                }
                
                if let result = result as? Dictionary<String,String> {
                    
                    let userId = result["id"]!
                    let myUser:PFUser = PFUser.current()!
                    let userFirstName = result["first_name"]
                    let userLastName = result["last_name"]
                    let userEmail = result["email"]
                    
                    if let userFirstName = userFirstName {
                        myUser.setValue(userFirstName, forKey: "firstName")
                    }
                    if let userLastName = userLastName {
                        myUser.setValue(userLastName, forKey: "lastName")
                    }
                    if let userEmail = userEmail {
                        myUser.setObject(userEmail, forKey: "email")
                    }
                
                    let pictureUrl = userPictureFaceboookUrl(userId: userId)!
                    guard let data = try? Data(contentsOf: pictureUrl) else {
                        return
                    }
                    let pictureFile = PFFile(data: data, contentType: "image/jpeg")
                    myUser.setObject(pictureFile, forKey: "picture")
                    
                    myUser.saveInBackground(block: { (success, error) in
                        if let error = error {
                            print(error.localizedDescription)
                            return
                        }
                        if success {
                            print("User saved")
                            User.fillCurrent()
                            if User.current != nil {
                                callback?()
                            }
                        }
                    })
                }
            })
            
        })
        
    }
    
    class func signOut(callback:(()->Void)?) {
//        guard let myUser = PFUser.current() else {
//            return
//        }
        PFUser.logOutInBackground { (error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            User.current = nil
            callback?()
        }
    }
    
    class func fillCurrent() {
        //TODO: Incapsulate parse
        guard let myUser = PFUser.current() else {
            return
        }
        guard let currentUser = createWith(myUser) else {
            print("Can't create User!")
            PFUser.logOut()
            return
        }
        User.current = currentUser
    }
    
    class func createWith(_ pfUser:PFUser) -> User? {
        _ = try? pfUser.fetchIfNeeded()
        let userId = pfUser.objectId!
        let userFirstName = pfUser["firstName"]
        let userLastName = pfUser["lastName"]
        let userEmail = pfUser["email"]
        var pictureUrl:URL?
        if let pictureFile = pfUser["picture"] as? PFFile {
            pictureUrl = URL(string: pictureFile.url!)
        }
        let dictUser = ["id":userId,
                        "firstName":userFirstName,
                        "lastName":userLastName,
                        "email":userEmail,
                        "pictureUrl":pictureUrl,
                        "location":""] as [String : Any?]
        guard let currentUser = User.init(dictUser) else {
            return nil
        }
        return currentUser
    }
}
