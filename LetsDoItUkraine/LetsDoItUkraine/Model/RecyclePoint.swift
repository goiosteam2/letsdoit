//
//  RecyclePoint.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 04.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import CoreLocation

enum RubbishType: Int, CustomStringConvertible {
    
    case all = 0, plastic = 1, paper = 2, glass = 3, mercury = 4, battery = 5, garbage = 6, polyethylene = 7, mixed = 8
    
    var description: String {
        switch self {
        case .plastic:
            return "Пластик"
        case .paper:
            return "Макулатура"
        case .glass:
            return "Стеклобой"
        case .mercury:
            return "Ртуть"
        case .battery:
            return "Батарейки"
        case .garbage:
            return "Старые вещи"
        case .polyethylene:
            return "Полиэтилен"
        case .mixed:
            return "Разное"
        case .all:
            return "Все пункты"
            
        }
    }
    
    var picture: UIImage {
        switch self {
        case .plastic:
            return UIImage.init(named: "Plastic")!
        case .paper:
            return UIImage.init(named: "Paper")!
        case .glass:
            return UIImage.init(named: "Glass")!
        case .mercury:
            return UIImage.init(named: "Mercury")!
        case .battery:
            return UIImage.init(named: "Battery")!
        case .garbage:
            return UIImage.init(named: "Garbage")!
        case .polyethylene:
            return UIImage.init(named: "Polyethylene")!
        case .mixed:
            return UIImage.init(named: "Mixed")!
        case .all:
            return UIImage.init(named: "All")!
            
        }
    }
    
    var pictureSelected: UIImage {
        switch self {
        case .plastic:
            return UIImage.init(named: "Plastic_Selected")!
        case .paper:
            return UIImage.init(named: "Paper_Selected")!
        case .glass:
            return UIImage.init(named: "Glass_Selected")!
        case .mercury:
            return UIImage.init(named: "Mercury_Selected")!
        case .battery:
            return UIImage.init(named: "Battery_Selected")!
        case .garbage:
            return UIImage.init(named: "Garbage_Selected")!
        case .polyethylene:
            return UIImage.init(named: "Polyethylene_Selected")!
        case .mixed:
            return UIImage.init(named: "Mixed_Selected")!
        case .all:
            return UIImage.init(named: "All_Selected")!
            
        }
    }
    
    var pinIcon: UIImage {
        switch self {
        case .plastic:
            return UIImage.init(named: "Plastic-pin")!
        case .paper:
            return UIImage.init(named: "Paper-pin")!
        case .glass:
            return UIImage.init(named: "Glass-pin")!
        case .mercury:
            return UIImage.init(named: "Mercury-pin")!
        case .battery:
            return UIImage.init(named: "Battery-pin")!
        case .garbage:
            return UIImage.init(named: "Garbage-pin")!
        case .polyethylene:
            return UIImage.init(named: "Polyethylene-pin")!
        case .mixed:
            return UIImage.init(named: "Mixed-pin")!
        case .all:
            return UIImage.init(named: "All-pin")!
            
        }
    }
    
    static var allTypes: [RubbishType] {
        var rubbishTypesAll = [RubbishType]()
        rubbishTypesAll.append(.plastic)
        rubbishTypesAll.append(.battery)
        rubbishTypesAll.append(.garbage)
        rubbishTypesAll.append(.glass)
        rubbishTypesAll.append(.mercury)
        rubbishTypesAll.append(.mixed)
        rubbishTypesAll.append(.paper)
        rubbishTypesAll.append(.polyethylene)
        rubbishTypesAll.append(.all)
        return rubbishTypesAll
    }
    
}

struct RecyclePoint:Equatable, MapPointProtocol {
    
    let id: String?
    let categories: Array<RubbishType>?
    let location: CLLocationCoordinate2D?
    let title: String?
    let weight: String?
    let phone: String?
    let website: URL?
    let logo: URL?
    let photo: URL?
    let address: String?
    let schedule: String?
    let summary: String?
    
    var categoriesText:String {
        guard let categories = categories else {
            return ""
        }
        var resStr = ""
        var first = true
        for item in categories {
            if first {
                first = false
                resStr += item.description
            } else {
                resStr += ", " + item.description
            }
        }
        return resStr
    }
    
    //MARK: MapPointProtocol
    
    var icon: URL? {
        return logo
    }
    var heading: String? {
        return title
    }
    var subHeading: String? {
        return categoriesText
    }
    var designation: String? {
        return address
    }
    var subtitle: String? {
        return schedule
    }
    var position: CLLocationCoordinate2D? {
        return location
    }
    
    //MARK: Equatable
    public static func ==(lhs: RecyclePoint, rhs: RecyclePoint) -> Bool {
        return lhs.id == rhs.id
    }
}

class RecyclePoints: MapPoints {
    
    static let shared = RecyclePoints()
    
    var current: Array<RecyclePoint> = []

    private init() {
    }

    //MARK: MapPoints
    
    typealias PointType = RecyclePoint
    
    func clearCurrent() {
        DispatchQueue.main.async(flags: .barrier) {
            self.current.removeAll()
        }
    }
}
struct rubbishTypeStructure {
    let rubbishType: RubbishType
    var selected:Bool
}

class SelectedRubbishTypes {
    static let shared = SelectedRubbishTypes()
    var current: Array<rubbishTypeStructure> = []
    private init () {
        var allRubbishTypes = [rubbishTypeStructure]()
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .plastic, selected: true))
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .paper, selected: true))
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .glass, selected: true))
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .mercury, selected: true))
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .battery, selected: true))
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .garbage, selected: true))
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .polyethylene, selected: true))
        allRubbishTypes.append(rubbishTypeStructure(rubbishType: .mixed, selected: true))
        self.current = allRubbishTypes
    }
}
