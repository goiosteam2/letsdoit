//
//  Settings.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 14.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

// MARK: - View Controllers

weak var recycleViewController = UIStoryboard( name: "Map", bundle: nil).instantiateViewController(withIdentifier: "RecycleMapViewController") as? RecycleMapViewController

weak var cleaningsViewController = UIStoryboard( name: "Map", bundle: nil).instantiateViewController(withIdentifier: "CleaningsViewController") as? CleaningsMapViewController

weak var cleaningCreationViewController = UIStoryboard( name: "CleaningCreation", bundle: nil).instantiateViewController(withIdentifier: "CleaningCreationViewController") as? CleaningCreationViewController

weak var detailedProfileViewController = UIStoryboard( name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "DetailedProfileViewController") as? DetailedProfileViewController

weak var detailedCleaningViewController = UIStoryboard( name: "Map", bundle: nil).instantiateViewController(withIdentifier: "DetailedCleaningViewController") as? DetailedCleaningViewController

weak var requestViewController = UIStoryboard( name: "CleaningCreation", bundle: nil).instantiateViewController(withIdentifier: "RequestViewController") as? RequestViewController

weak var tabBarViewController = UIStoryboard( name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController

weak var registrationProfileViewController = UIStoryboard( name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "RegistrationProfileViewController") as? RegistrationProfileViewController

weak var fullFilterViewController = UIStoryboard( name: "Map", bundle: nil).instantiateViewController(withIdentifier: "FullFilterViewController") as? FullFilterViewController

//MARK: - Api keys

// Google
let kGMSApiKey  = "AIzaSyDJCSeu0EwnGFLa4tPGFNA0taPFMxzPXn8"

//Parse 
let kParseApplicationId = "04a7a3ca86e56af69d5fa30eb1e079526963c219"
let kParseClientKey = "d1d6a95bd8f38c2de25fb667604c6bc7048ed4ef"
let kParseServer = "http://ec2-52-34-146-243.us-west-2.compute.amazonaws.com:80/parse"

//Zendesk
let kZendeskAppID = "1d56520bc0bad4add8fbf0e8541810c13eaa451fd65c3995"

//MARK: - Notifications

let searchResultNotification = Notification.Name(rawValue:"SearchResultNotification")
let recycleFilterApplyNotification = Notification.Name(rawValue:"RecycleFilterApplyNotification")

//MARK: Google maps

let defaultZoom:Float = 16

//MARK: Facebook
func userPictureFaceboookUrl(userId:String) -> URL? {
    return URL(string:"https://graph.facebook.com/\(userId)/picture?type=large")
}

//MARK: - Colours



