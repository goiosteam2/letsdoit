//
//  NewsViewController.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 01.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import ZendeskSDK

class NewsViewController: UIViewController, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate {

    
    @IBOutlet weak var newsCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var suggestNewsButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadNews()
        
        collectionViewFlowLayout.itemSize.width = UIScreen.main.bounds.size.width
        newsCollectionView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func SuggestNewsBtnTap(_ sender: AnyObject) {
        ZDKRequests.presentRequestCreation(with: self)
    }

    func loadNews() {
        DataBaseService.shared.getNews { 
            self.newsCollectionView.reloadData()
        }                
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return News.shared.news.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellForNews", for: indexPath) as! NewsCollectionViewCell
        cell.pieceOfNews = News.shared.news[indexPath.row]
        cell.parentVC = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedpieceOfNews = (newsCollectionView.cellForItem(at: indexPath) as! NewsCollectionViewCell).pieceOfNews
        self.performSegue(withIdentifier: "showDetailNews", sender: selectedpieceOfNews)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! DetailNewsViewController
        vc.pieceOfNews = sender as! PieceOfNews
        segue.destination.az_modalTransition = Transition()
    }
}
