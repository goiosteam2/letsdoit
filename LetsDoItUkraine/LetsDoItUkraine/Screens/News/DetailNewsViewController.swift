//
//  DetailNewsViewController.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 05.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class DetailNewsViewController: UIViewController {

    @IBOutlet weak var pictureImage: UIImageView!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pictureHeightConstraint: NSLayoutConstraint!
    
    var pieceOfNews:PieceOfNews!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.pieceOfNews.picture == nil{
            pictureHeightConstraint.constant = 0
        }
        self.pictureImage.sd_setImage(with: pieceOfNews.picture)
        self.titleLabel.text = pieceOfNews.title
        self.bodyLabel.text = pieceOfNews.body
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.locale = Locale(identifier: "ru_RU")
        self.dateLabel.text = dateFormatter.string(from: pieceOfNews.date)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func shareButtonTap(_ sender: AnyObject) {
        let arr:[Any] = [self.pieceOfNews.link as Any]
        let vc = UIActivityViewController(activityItems: arr, applicationActivities: nil)
        vc.excludedActivityTypes = [UIActivityType.airDrop,
                                    UIActivityType.print,
                                    UIActivityType.assignToContact,
                                    UIActivityType.saveToCameraRoll,
                                    UIActivityType.postToFlickr,
                                    UIActivityType.postToVimeo]
        self.present(vc, animated: true, completion: nil)
        
    }
    

}
