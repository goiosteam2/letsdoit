//
//  RequestViewController.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 30.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class RequestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func checkMailButtonTapped(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: "message://")!, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }

    @IBAction func okButtonTapped(_ sender: UIButton) {
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }

}
