//
//  NewsCollectionViewCell.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 05.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pictureImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var parentVC: UIViewController!
    
    @IBOutlet weak var pictureHeightConstraint: NSLayoutConstraint!
    var pieceOfNews:PieceOfNews! {
        didSet {
            self.pictureImage.sd_setImage(with: pieceOfNews.picture)
            self.titleLabel.text = pieceOfNews.title
            self.bodyLabel.text = pieceOfNews.body
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            dateFormatter.locale = Locale(identifier: "ru_RU")
            //dateFormatter.setLocalizedDateFormatFromTemplate("ru")
            self.dateLabel.text = dateFormatter.string(from: pieceOfNews.date)
            
            if pieceOfNews.picture == nil {
                pictureHeightConstraint.constant = 0
            }
        }
    }
    
    
    @IBAction func shareButtonTap(_ sender: AnyObject) {
        let arr = [self.pieceOfNews.link]
        let vc = UIActivityViewController(activityItems: arr, applicationActivities: nil)
        vc.excludedActivityTypes = [UIActivityType.airDrop,
                                    UIActivityType.print,
                                    UIActivityType.assignToContact,
                                    UIActivityType.saveToCameraRoll,
                                    UIActivityType.postToFlickr,
                                    UIActivityType.postToVimeo]
        self.parentVC.present(vc, animated: true, completion: nil)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}
