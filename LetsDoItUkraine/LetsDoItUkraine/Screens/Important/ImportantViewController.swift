//
//  ImportantViewController.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 13.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class ImportantViewController: UIViewController {

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

}
