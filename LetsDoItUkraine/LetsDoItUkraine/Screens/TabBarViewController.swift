//
//  TabBarViewController.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 30.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import PKHUD

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {

    open var transitionAnimation = BottomNavigationTransitionAnimation.fade
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    public init(viewControllers: [UIViewController]) {
        super.init(nibName: nil, bundle: nil)
        self.viewControllers = viewControllers
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        selectedIndex = 1
        prepare()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if self.viewControllers?.index(of: viewController) == 2 {
            if let _ = User.current {
                cleaningCreationViewController!.az_modalTransition = Transition()
                present(cleaningCreationViewController!, animated: true, completion: nil)
            } else {
                selectedIndex = 0
                HUD.flash(.labeledError(title:"Создание уборки", subtitle:"Необходима регистрация"), delay: 2.0)
            }
            return false
        } else if self.viewControllers?.index(of: viewController) == 0 {
            return viewController != tabBarController.selectedViewController
        }
        return true
    }
    
    open func prepare() {
        view.clipsToBounds = true
        view.contentScaleFactor = UIScreen.main.scale
        view.backgroundColor = .white
        delegate = self
    }
    
    // Handles transitions when tabBarItems are pressed.
    open func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let fVC: UIViewController? = fromVC
        let tVC: UIViewController? = toVC
        if nil == fVC || nil == tVC {
            return nil
        }
        return .fade == transitionAnimation ? BottomNavigationFadeAnimatedTransitioning() : nil
    }
    
}

public class BottomNavigationFadeAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromView : UIView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let toView : UIView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        toView.alpha = 0
        
        transitionContext.containerView.addSubview(fromView)
        transitionContext.containerView.addSubview(toView)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       animations: { _ in
                        toView.alpha = 1
                        fromView.alpha = 0
        }) { _ in
            transitionContext.completeTransition(true)
        }
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.35
    }
}

@objc(BottomNavigationTransitionAnimation)
public enum BottomNavigationTransitionAnimation: Int {
    case none
    case fade
}
