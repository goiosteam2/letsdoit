//
//  ProfileViewController.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 03.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class RegistrationProfileViewController: UIViewController {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.photoView.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if let currentUser = User.current {
            navigationController?.pushViewController(detailedProfileViewController!, animated: false)
            if let pictureUrl = currentUser.pictureUrl {
                self.photoView.sd_setImage(with: pictureUrl)
            }
            self.fullNameTextField.text = currentUser.fullName
        } else {
            self.photoView.image = UIImage(named: "profile-placeholder")
            self.fullNameTextField.text = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didTouchLoginButton(_ sender: UIButton) {
        User.signIn { [weak self] in
            guard let currentUser = User.current else {
                return
            }
            
            self?.performSegue(withIdentifier: "showDetailedProfile", sender: nil)
            
            if let pictureUrl = currentUser.pictureUrl {
                self?.photoView.sd_setImage(with: pictureUrl)
            }
            self?.fullNameTextField.text = currentUser.fullName
        }
    }

}
