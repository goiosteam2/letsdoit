//
//  DetailedProfileViewController.swift
//  LetsDoItUkraine
//
//  Created by ArtemMy on 02.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import PKHUD

class DetailedProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var noCurrentCleaningImage: UIImageView!
    @IBOutlet weak var noCurrentCleaningLabel: UILabel!
    
    @IBOutlet weak var volunteerCountLabel: UILabel!
    @IBOutlet weak var coordinatorCountLabel: UILabel!
    
    @IBOutlet weak var locationcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var currentCleaningAddress: UILabel!
    @IBOutlet weak var DetailedCurrentCleaningButton: UIButton!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cleaningHistoryTableView: UITableView!
    @IBOutlet weak var profileTabBar: UITabBarItem!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if let user = User.current {
            if user.cleaningsHistory.count == 0 {
                return 1
            } else {
                return user.cleaningsHistory.count
            }
        } else {
            return 1
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let user = User.current {
            if user.cleaningsHistory.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "noCleaningHistory", for: indexPath)
                return cell
            } else  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cleaningHistory", for: indexPath) as! CleaningTableViewCell
                let cleaning = User.current!.cleaningsHistory[indexPath.row]
                cell.address.text = cleaning.address
                let dateFormatter: DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MM yyyy hh:mm"
                cell.dateLabel.text = dateFormatter.string(from: cleaning.datetime!)
                cell.titleLabel.text = cleaning.title
                return cell
            }
        }
        return UITableViewCell()
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
//        let height = self.cleaningHistoryTableView.rowHeight;
//        if (user have history)
//        height *= myArray.count;
//        self.tableViewHeightConstraint.constant = height
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItems = nil
        self.view.layoutIfNeeded()
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width / 2
        
        if let currentUser = User.current {
            if let pictureUrl = currentUser.pictureUrl {
                self.profilePicture.sd_setImage(with: pictureUrl)
            }
            self.name.text = currentUser.fullName
            self.location.text = currentUser.location
            DataBaseService.shared.currentCleaning(callback: { (cleaning, error) in
                if let cleaning = cleaning {
                    self.locationcon.isHidden = false
                    self.titleLabel.isHidden = false
                    self.currentCleaningAddress.isHidden = false
                    self.DetailedCurrentCleaningButton.isHidden = false
                    self.noCurrentCleaningImage.isHidden = true
                    self.noCurrentCleaningLabel.isHidden = true
                    self.titleLabel.text = cleaning.title
                    self.currentCleaningAddress.text = cleaning.address
                }
            })
            
            HUD.show(.progress)
            DataBaseService.shared.cleaningHistory(callback: { (cleanings, error) in
                self.cleaningHistoryTableView.reloadData()
                HUD.hide()
            })
            
            DataBaseService.shared.coordinatorHistory(callback: { (number, error) in
                if !error {
                    self.coordinatorCountLabel.text = number.description
                }
                HUD.hide()
            })
            
            DataBaseService.shared.volunteerHistory(callback: { (number, error) in
                if !error {
                    self.volunteerCountLabel.text = number.description
                }
                HUD.hide()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func didTouchCurrentCleaningButton(_ sender: UIButton) {
        if let cleaning = User.current?.currentCleaning {
            detailedCleaningViewController!.az_modalTransition = Transition()
            detailedCleaningViewController!.cleaning = cleaning
            navigationController?.pushViewController(detailedCleaningViewController!, animated: true)
        }
    }
    
    @IBAction func didTouchFindButton(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 1
    }

    @IBAction func createButtonTapped(_ sender: UIButton) {
        cleaningCreationViewController!.az_modalTransition = Transition()
        present(cleaningCreationViewController!, animated: true, completion: nil)
    }
    @IBAction func settingsButtonTapped(_ sender: Any) {
        guard let currentUser = User.current else {
            return
        }
        let settingsAlert = UIAlertController(title:"Вы вошли как:", message: currentUser.fullName, preferredStyle: .actionSheet)
        let action =  UIAlertAction(title: "Sign out", style: .default, handler: signOut)
        settingsAlert.addAction(action)
        settingsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(settingsAlert, animated:true, completion:nil)
    }
    
    func signOut(action:UIAlertAction) {
        User.signOut { [weak self] in
            if let _ = User.current {
                return
            }
            self?.navigationController?.viewControllers = [registrationProfileViewController!]
            self?.navigationController?.pushViewController(registrationProfileViewController!, animated: true)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        segue.destination.az_modalTransition = Transition()
    }

}
