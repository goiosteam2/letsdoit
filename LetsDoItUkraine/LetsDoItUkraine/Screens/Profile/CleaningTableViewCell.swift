//
//  CleaningTableViewCell.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 16.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class CleaningTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
