//
//  RecycleMapViewController.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 11.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import PKHUD

class RecycleMapViewController: UIViewController, GMSMapViewDelegate,
    CLLocationManagerDelegate, UICollectionViewDataSource,
    UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, FilterViewCellDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var shortInfoView: UIView!
    @IBOutlet weak var shortInfoCollectionView: UICollectionView!
    @IBOutlet weak var shortInfoCollectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var quickFilterCollectionView: UICollectionView!
    @IBOutlet weak var searchWithinBox: UIButton!

    var markers:Array<GMSMarker> = []
    var cameraZoom = defaultZoom - 6
    var searchState = true {
        didSet{
            searchWithinBox.isHidden = searchState
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
//        quickFilterCollectionView.delegate = self

//        updateMap()
        
        shortInfoCollectionView.register(UINib.init(nibName: "ShortInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShortInfoCollectionViewCell")
        quickFilterCollectionView.register(UINib.init(nibName: "QuickFilterViewCell", bundle: nil), forCellWithReuseIdentifier: "QuickFilterViewCell")
        
        if let location = CLLocationManager().location {
            mapView.animate(to: GMSCameraPosition.camera(withTarget: location.coordinate, zoom: defaultZoom))
        }
        
        NotificationCenter.default.addObserver(forName:searchResultNotification,
                                               object:nil, queue:nil,
                                               using:catchSearchResult)
        NotificationCenter.default.addObserver(forName:recycleFilterApplyNotification,
                                               object:nil, queue:nil,
                                               using:applyFilter)
        
        shortInfoCollectionViewFlowLayout.itemSize.width = UIScreen.main.bounds.size.width - 20
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        searchState = false
    }
    
    func updateMap() {
        HUD.show(.progress)
        let visibleRegion = self.mapView.projection.visibleRegion()
        let filter = SelectedRubbishTypes.shared.current.filter({$0.selected}).map({$0.rubbishType.rawValue})
        DataBaseService.shared.recyclePointsWithinGeoBox(fromSouthwest: visibleRegion.nearLeft, toNortheast: visibleRegion.farRight, filter: filter){
            self.showRecycleMap()
            self.shortInfoView.isHidden = true
            HUD.flash(.progress, delay: 1.0, completion: { (success) in
                self.shortInfoCollectionView.reloadData()
            })
        }
        self.searchState = true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let recyclePoints = RecyclePoints.shared.current
        mapView.animate(to: GMSCameraPosition.camera(withTarget: marker.position, zoom: mapView.camera.zoom))
        if let index = recyclePoints.index(of: marker.userData as! RecyclePoint) {
            let indexPath = IndexPath(item: index, section: 0)
            shortInfoCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
        }
        shortInfoViewHide(false)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        shortInfoViewHide(true)
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case shortInfoCollectionView:
            return RecyclePoints.shared.current.count
        case quickFilterCollectionView:
            return SelectedRubbishTypes.shared.current.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case shortInfoCollectionView:
            let recyclePoints = RecyclePoints.shared.current
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShortInfoCollectionViewCell", for: indexPath as IndexPath) as! ShortInfoCollectionViewCell
            cell.point(recyclePoints[indexPath.item])
            return cell
        case quickFilterCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuickFilterViewCell", for: indexPath as IndexPath) as! FilterViewCell
            let currentType = SelectedRubbishTypes.shared.current[indexPath.row]
            cell.rubbishType = currentType.rubbishType
            cell.isSelectedItem = currentType.selected
            cell.delegate = self
            return cell
        default:
            let cell = UICollectionViewCell.init()
            return cell
        }
    }

    //MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let recyclePoints = RecyclePoints.shared.current
        let recyclePoint = recyclePoints[indexPath.row]
        performSegue(withIdentifier: "DetailedRecyclePoint", sender: recyclePoint)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let marker = markers[indexPath.row]
        mapView.selectedMarker = marker
        let update = GMSCameraUpdate.setCamera(GMSCameraPosition.camera(withTarget: marker.position, zoom: defaultZoom))
        mapView.animate(with: update)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        let marker = markers[indexPath.row]
//        mapView.selectedMarker = marker
//        let updateCamera = GMSCameraUpdate.setCamera(GMSCameraPosition.camera(withTarget: marker.position, zoom: defaultZoom))
//        mapView.animate(with: updateCamera)
    }

    //MARK: UICollectionViewDelegateFlowLayout
    

    
 
    //MARK: - Actions 
    
    @IBAction func searchWithinBox(_ sender: UIButton) {
        updateMap()
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailedRecyclePoint"
        {
            if let destinationVC = segue.destination as? DetailedRecyclePointViewController {
                destinationVC.recyclePoint = sender as! RecyclePoint!
            }
        }
    }
    
    //MARK: FilterViewCellDelegate
    
    func didTapCellButton(cell: FilterViewCell) {
        cell.isSelectedItem = !(cell.isSelectedItem)
        let i = SelectedRubbishTypes.shared.current.index(where: {$0.rubbishType == cell.rubbishType})!
        SelectedRubbishTypes.shared.current[i].selected = cell.isSelectedItem
        updateMap()
    }
    
    //MARK: - support functions
    
    func showRecycleMap() {
        
        mapView.clear()
        
        for point in RecyclePoints.shared.current {
            let marker = GMSMarker()
            marker.position =  point.location!
            marker.title = point.title
            marker.snippet = point.address
            marker.iconView = UIImageView(image: point.categories?.first?.pinIcon)
            marker.iconView?.contentScaleFactor = 2.0
            marker.userData = point
            marker.appearAnimation = kGMSMarkerAnimationPop
            marker.map = mapView
            markers.append(marker)
        }
        shortInfoViewHide(true)
    }

    func catchSearchResult(notification:Notification) -> Void {
        guard let userInfo = notification.userInfo,
            let classType = userInfo["classType"] as? AnyClass,
            classType == self.classForCoder,
            let place = userInfo["place"] as? GMSPlace else {
                return
        }
        mapView.animate(to: GMSCameraPosition.camera(withTarget: place.coordinate, zoom: defaultZoom))
        let searchMarker = GMSMarker()
        searchMarker.position =  place.coordinate
        searchMarker.title = place.name
        searchMarker.snippet = place.formattedAddress
        searchMarker.userData = place
        searchMarker.appearAnimation = kGMSMarkerAnimationPop
        searchMarker.map = mapView
        mapView.selectedMarker = searchMarker
        
    }
    
    func shortInfoViewHide(_ isHidden:Bool) {
        UIView.transition(with: shortInfoView, duration: 1, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            self.shortInfoView.isHidden = isHidden
            }, completion: nil)
        UIView.animate(withDuration: 1) {
            self.mapView.padding = isHidden ? UIEdgeInsets() : UIEdgeInsetsMake(100, 0, 100, 0)
            self.shortInfoView.isHidden = isHidden
        }
    }
    
    func applyFilter(notification:Notification) {
        quickFilterCollectionView.reloadData()
        updateMap()
    }

}
