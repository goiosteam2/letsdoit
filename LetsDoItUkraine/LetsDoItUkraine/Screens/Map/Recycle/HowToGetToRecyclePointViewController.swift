//
//  HowToGetToRecyclePointViewController.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 13.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import GoogleMaps

class HowToGetToRecyclePointViewController: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var recyclePoint:RecyclePoint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let coords = CLLocationCoordinate2DMake(recyclePoint.location!.latitude, recyclePoint.location!.longitude)
        mapView.animate(to: GMSCameraPosition.camera(withTarget: coords, zoom: defaultZoom))
        let marker = GMSMarker()
        marker.position =  recyclePoint.location!
        marker.title = recyclePoint.title
        marker.snippet = recyclePoint.address
        marker.iconView = UIImageView(image: recyclePoint.categories?.first?.pinIcon)
        marker.iconView?.contentScaleFactor = 2.0
        marker.userData = recyclePoint
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.map = mapView
        mapView.selectedMarker = marker
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
