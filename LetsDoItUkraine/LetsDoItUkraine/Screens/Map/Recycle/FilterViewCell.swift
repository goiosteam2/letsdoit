//
//  QuickFilterViewCell.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 21.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

protocol FilterViewCellDelegate : class {
    func didTapCellButton(cell : FilterViewCell)
}


class FilterViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var image: UIImageView!
    weak var delegate:FilterViewCellDelegate!
    var isSelectedItem:Bool = false {
        didSet {
            self.image.image = self.isSelectedItem ? rubbishType.pictureSelected : rubbishType!.picture.withRenderingMode(.alwaysTemplate)
            self.title.textColor = self.isSelectedItem ? UIColor.black : UIColor.lightGray
        }
    }
    var rubbishType:RubbishType! {
        didSet {
            self.title.text = rubbishType!.description
            //self.image.image = self.isSelectedItem ? rubbishType.pictureSelected : rubbishType!.picture
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //func didTapCellButton (cell : FilterViewCell) {
    //    delegate.didTapCellButton(cell: self)
    //}

    @IBAction func didTapCellButton(_ sender: AnyObject) {
        delegate.didTapCellButton(cell: self)
    }
    
}
