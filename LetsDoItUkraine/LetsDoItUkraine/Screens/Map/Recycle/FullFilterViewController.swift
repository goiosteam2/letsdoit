//
//  FullFilterViewController.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 26.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class FullFilterViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, FilterViewCellDelegate {
    
    var rubbishTypesAll = RubbishType.allTypes
    var tempSelectedRubbishTypes: Set<RubbishType> = []
    
    @IBOutlet weak var fullFilterCollectionView: UICollectionView!
    
    @IBOutlet weak var FullFilterCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tempSelectedRubbishTypes = Set(SelectedRubbishTypes.shared.current.filter({$0.selected}).map({$0.rubbishType}))
        
        let screenWidth = UIScreen.main.bounds.size.width
        FullFilterCollectionViewFlowLayout.itemSize.width = round(screenWidth * 0.9 / 3)
        FullFilterCollectionViewFlowLayout.itemSize.height = FullFilterCollectionViewFlowLayout.itemSize.width
        let distance = round((screenWidth - FullFilterCollectionViewFlowLayout.itemSize.width * 3) / 6)
        FullFilterCollectionViewFlowLayout.sectionInset.bottom = distance
        FullFilterCollectionViewFlowLayout.sectionInset.top = distance
        FullFilterCollectionViewFlowLayout.sectionInset.left = distance
        FullFilterCollectionViewFlowLayout.sectionInset.right = distance
        FullFilterCollectionViewFlowLayout.minimumInteritemSpacing = distance
        FullFilterCollectionViewFlowLayout.minimumLineSpacing = distance
        
        fullFilterCollectionView.register(UINib.init(nibName: "FullFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FullFilterCollectionViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeWithSelectedItems(_ sender: AnyObject) {
        var tempArr = [rubbishTypeStructure]()
        for ss in SelectedRubbishTypes.shared.current {
            tempArr.append(rubbishTypeStructure(rubbishType: ss.rubbishType, selected: tempSelectedRubbishTypes.contains(ss.rubbishType)))
        }
        SelectedRubbishTypes.shared.current.removeAll()
        SelectedRubbishTypes.shared.current.append(contentsOf: tempArr)
        NotificationCenter.default.post(name: recycleFilterApplyNotification, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func closeWithoutChanges(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FullFilterCollectionViewCell", for: indexPath as IndexPath) as! FilterViewCell
        cell.rubbishType = rubbishTypesAll.removeFirst()
        let currentType = SelectedRubbishTypes.shared.current.filter({$0.rubbishType == cell.rubbishType}).first
        cell.isSelectedItem = currentType == nil ? false : currentType!.selected
        cell.delegate = self
        return cell
    }
    
    func didTapCellButton(cell: FilterViewCell) {
        if (cell.rubbishType == RubbishType.all) {
            cell.isSelectedItem = !(cell.isSelectedItem)
            for item in fullFilterCollectionView.indexPathsForVisibleItems {
                let filterCell = fullFilterCollectionView.cellForItem(at: item) as! FilterViewCell
                if filterCell.rubbishType != .all {
                    addItemToSelectedRubbishTypes(cell: filterCell, insert: cell.isSelectedItem)
                    filterCell.isSelectedItem = cell.isSelectedItem
                }
            }
        } else {
            cell.isSelectedItem = !(cell.isSelectedItem)
            addItemToSelectedRubbishTypes(cell: cell, insert: cell.isSelectedItem)
        }
    }

    func addItemToSelectedRubbishTypes(cell: FilterViewCell, insert: Bool){
        if insert {
            tempSelectedRubbishTypes.insert(cell.rubbishType)
        } else {
            tempSelectedRubbishTypes.remove(cell.rubbishType)
        }
        cell.isSelectedItem = insert
    }

}
