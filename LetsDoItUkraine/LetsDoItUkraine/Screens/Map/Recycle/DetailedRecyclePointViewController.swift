//
//  DetailedRecclePointViewController.swift
//  LetsDoItUkraine
//
//  Created by ArtemMy on 16.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class DetailedRecyclePointViewController: UIViewController {
    
    var recyclePoint:RecyclePoint!

    
    @IBOutlet weak var mapPhotoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var phone: UITextView!
    @IBOutlet weak var site: UITextView!
    @IBOutlet weak var mapPhoto: UIImageView!
    @IBOutlet weak var recyclePointLogo: UIImageView!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var garbageTypes: UILabel!
    @IBOutlet weak var receptionWeight: UILabel!
    @IBOutlet weak var companyAddress: UILabel!
    @IBOutlet weak var timeOfReceipt: UILabel!
    @IBOutlet weak var detailedGrbageInformation: UILabel!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.site.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right:0)
        self.phone.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right:0);
        self.view.layoutIfNeeded()
        self.recyclePointLogo.layer.cornerRadius = self.recyclePointLogo.frame.size.width / 2
        self.recyclePointLogo.layer.borderWidth = 1;
        self.recyclePointLogo.layer.borderColor = UIColor.lightGray.cgColor
        
        setRecyclePointDetails()
        
        let backButton =  UIBarButtonItem(image: UIImage(named:"Pin-Left")!, style: .plain, target: self, action: #selector(self.backButtonTapped))
        navigationItem.leftBarButtonItem = backButton
    }
    
    func setRecyclePointDetails(){
        if let photoUrl = recyclePoint.photo {
            self.mapPhoto.sd_setImage(with: photoUrl, completed: { [weak self] (_, _, _, _) in
                self?.mapPhotoHeightConstraint.constant = 128
                UIView.animate(withDuration: 0.5) {
                    self?.view.layoutIfNeeded()
                }
            })
        }
        if let logoUrl = recyclePoint.logo {
            self.recyclePointLogo.sd_setImage(with: logoUrl)
        }
        self.companyName.text = recyclePoint.title
        self.phone.text = recyclePoint.phone
        self.site.text = recyclePoint.website?.absoluteString
        
        self.garbageTypes.text = ""
        for element in recyclePoint.categories!{
            self.garbageTypes.text! += "\(element)\n"
        }
        self.garbageTypes.text!.characters.removeLast()
        
        self.receptionWeight.text = recyclePoint.weight
        self.companyAddress.text = recyclePoint.address
        self.timeOfReceipt.text = recyclePoint.schedule
        self.detailedGrbageInformation.text = recyclePoint.summary
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func backButtonTapped() {
        _ = navigationController?.popViewController(animated: true)
    }

    // MARK: - Navigation

    
    @IBAction func howToGetToPointTap(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "howToGetToPointSegue", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.az_modalTransition = Transition()
        let vc = segue.destination as! HowToGetToRecyclePointViewController
        vc.recyclePoint = recyclePoint
    }
    
}
