//
//  MapPageViewController.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 14.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import GooglePlaces

class MapPageViewController: UIPageViewController, UIPageViewControllerDelegate, UISearchBarDelegate {
   
    var locationManager = CLLocationManager()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    //MARK: - Actions
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        setViewcontroller(at: sender.selectedSegmentIndex)
        let filterButton = UIBarButtonItem(image: UIImage(named:"filters"), style: .plain, target: self, action: #selector(filterButtonTapped))
        navigationItem.leftBarButtonItem = sender.selectedSegmentIndex == 0 ? filterButton : nil
    }
    
    @IBAction func searchButton(_ sender: AnyObject?) {
        let acController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        acController.autocompleteFilter = filter
        acController.delegate = self.viewControllers?.last as? GMSAutocompleteViewControllerDelegate
        acController.az_modalTransition = Transition()
        self.present(acController, animated: true) {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self.viewControllers?.last as? CLLocationManagerDelegate
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        setViewcontroller(at: 0)
        let filterButton = UIBarButtonItem(image: UIImage(named:"filters"), style: .plain, target: self, action: #selector(filterButtonTapped))
        navigationItem.leftBarButtonItem = filterButton
    }
    
    //MARK: - UIPageViewController
    
    func mapViewController(at index:Int) -> UIViewController {
        return index == 0 ? recycleViewController! : cleaningsViewController!
    }
    
    func setViewcontroller(at index: Int) {
        setViewControllers([mapViewController(at: index)], direction: (index == 0 ? .reverse : .forward), animated: true, completion: nil)
    }
    
    //MARK: - UIPageViewControllerDelegate
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    }
    
    //MARK: - UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            searchButton(nil)
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchButton(nil)
    }
    
    func filterButtonTapped() {
        fullFilterViewController!.az_modalTransition = Transition()
        self.present(fullFilterViewController!, animated: true, completion: nil)
    }
 
}

extension UIViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    public func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        NotificationCenter.default.post(name:searchResultNotification, object: nil, userInfo:["place":place, "classType":self.classForCoder])
        self.dismiss(animated: true, completion: nil)
    }
    
    public func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error.localizedDescription)")
        self.dismiss(animated: true, completion: nil)
    }
    
    // User canceled the operation.
    public func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        self.dismiss(animated: true, completion: nil)
    }
}
