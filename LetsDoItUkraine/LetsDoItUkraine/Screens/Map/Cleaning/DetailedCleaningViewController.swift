//
//  DetailedCleaningViewController.swift
//  LetsDoItUkraine
//
//  Created by ArtemMy on 22.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import PopupDialog
import PKHUD

class DetailedCleaningViewController: UIViewController {

    var cleaning:Cleaning! {
        didSet {
            print("Cleaning set: \(cleaning)")
        }
    }
    @IBOutlet weak var attendButton: UIButton!
    @IBOutlet var cleaningImages: [UIImageView]!
    @IBOutlet weak var cleaningTitle: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var coordinatorName: UILabel!
    @IBOutlet weak var phoneNumber: UITextView!
    @IBOutlet weak var emailAddress: UITextView!
    @IBOutlet weak var coordinatorCount: UILabel!
    @IBOutlet weak var volunteerCount: UILabel!
    @IBOutlet weak var activity: UILabel!
    @IBOutlet weak var cleaningAddress: UILabel!
    @IBOutlet weak var cleaningDateTime: UILabel!
    @IBOutlet weak var cleaningVolunteerCount: UILabel!
    @IBOutlet weak var cleaningDescription: UILabel!
    @IBOutlet weak var picture1View: UIImageView!
    @IBOutlet weak var picture2View: UIImageView!
    @IBOutlet weak var picture3View: UIImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneNumber.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right:0)
        emailAddress.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right:0)
        
        cleaningTitle.text = cleaning.title
        cleaningDescription.text = cleaning.about
        cleaningAddress.text = cleaning.address
        cleaningVolunteerCount.text = cleaning.numberOfPeople.description
        coordinatorName.text = cleaning.coordinator?.fullName
        phoneNumber.text = cleaning.coordinator?.phone
        emailAddress.text = cleaning.coordinator?.email
        
        if let pictureUrl = cleaning.coordinator?.pictureUrl {
            profileImage.sd_setImage(with: pictureUrl)
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyy, hh:mm"
        dateFormatter.locale  = Locale(identifier: "ru_RU")
        self.cleaningDateTime.text = dateFormatter.string(from: cleaning.datetime!)
        if let pictures = cleaning.pictures {
            for index in 0..<pictures.count {
                if let url = pictures[index] {
                    cleaningImages[index].sd_setImage(with: url)
                }
            }
            if pictures.count >= 1 {
                picture1View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.popImage)))
            }
            if pictures.count >= 2 {
                picture2View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.popImage)))
            }
            if pictures.count >= 3 {
                picture3View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.popImage)))
            }
        }
        
        DataBaseService.shared.coordinatorHistory(cleaning.coordinator!.id) { (number, error) in
            if !error {
                self.coordinatorCount.text = number.description
            }
        }
        
        DataBaseService.shared.volunteerHistory(cleaning.coordinator!.id) { (number, error) in
            if !error {
             self.volunteerCount.text = number.description
            }
        }
        
        DataBaseService.shared.currentCleaning(callback: { [weak self](cleaning, error) in
            if let _ = cleaning {
                self?.attendButton.isEnabled = true
                self?.attendButton.alpha = 1.0
            } else {
                self?.attendButton.isEnabled = false
                self?.attendButton.alpha = 0.5
            }
        })
        
        let backButton =  UIBarButtonItem(image: UIImage(named:"Pin-Left")!, style: .plain, target: self, action: #selector(self.backButtonTapped))
        navigationItem.leftBarButtonItem = backButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func backButtonTapped() {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func attendButtonTapped(_ sender: UIButton) {
        HUD.show(.progress)
        cleaning.addCurrentUser { [weak self, cleaning] (success:Bool)  in
            if success {
                self?.attendButton.isEnabled = false
                self?.attendButton.alpha = 0.5
                self?.cleaningVolunteerCount.text = (cleaning!.numberOfPeople+1).description
                HUD.flash(.success, delay: 1.0)
            }
        }
    }
    @IBAction func donateButtonTapped(_ sender: UIButton) {
        let url = URL(string: "http://www.letsdoit.ua")
        _ = UIApplication.shared.canOpenURL(url!)
    }
    
    func popImage(_ recognizer:UITapGestureRecognizer) {
        if let imageView = recognizer.view as? UIImageView,
            let image = imageView.image {
            let popup = PopupDialog(title: "", message: "", image: image, buttonAlignment: UILayoutConstraintAxis.horizontal, transitionStyle: PopupDialogTransitionStyle.zoomIn, gestureDismissal: true, completion: nil)
            let vc = popup.viewController as! PopupDialogDefaultViewController
            vc.view.contentMode = .scaleAspectFit
            self.present(popup, animated: true, completion: nil)
        }
    }

}
