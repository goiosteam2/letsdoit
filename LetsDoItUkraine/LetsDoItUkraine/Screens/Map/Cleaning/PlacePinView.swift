//
//  PlacePinView.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 08.11.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit

class PlacePinView: UIView {

    @IBOutlet weak var pinImageView: UIImageView!
    @IBOutlet weak var coreLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
