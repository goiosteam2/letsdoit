//
//  CleaningsMapViewController.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 14.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import PKHUD

class CleaningsMapViewController: UIViewController, GMSMapViewDelegate,
    CLLocationManagerDelegate, UICollectionViewDataSource,
    UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var shortInfoView: UIView!
    @IBOutlet weak var shortInfoCollectionView: UICollectionView!
    @IBOutlet weak var searchWithinBox: UIButton!
    
    var markers:Array<GMSMarker> = []
    var selectedMarker:GMSMarker?
    var searchState = true {
        didSet{
            searchWithinBox.isHidden = searchState
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let location = CLLocationManager().location {
            mapView.camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: defaultZoom)
        }
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true

//        updateMap()
        
        shortInfoCollectionView.register(UINib.init(nibName: "ShortInfoCollectionViewCell", bundle: nil),forCellWithReuseIdentifier: "ShortInfoCollectionViewCell")
        
        NotificationCenter.default.addObserver(forName:searchResultNotification,
                                               object:nil, queue:nil,
                                               using:catchSearchResult)
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        searchState = false
    }
    
    func updateMap() {
        HUD.show(.progress)
        let visibleRegion = self.mapView.projection.visibleRegion()
        DataBaseService.shared.cleaningsWithinGeoBox(fromSouthwest: visibleRegion.nearLeft, toNortheast: visibleRegion.farRight){
            self.showCleningsMap()
            self.shortInfoView.isHidden = true
            HUD.flash(.progress, delay: 1.0, completion: { (success) in
                self.shortInfoCollectionView.reloadData()
            })
        }
        self.searchState = true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let cleaning = marker.userData as? Cleaning else {
            return false
        }
        shortInfoViewHide(false)
        if let index = Cleanings.shared.current.index(of: cleaning) {
            let indexPath = IndexPath(item: index, section: 0)
            shortInfoCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
        }
        marker.zIndex = 1
        if let selectedMarker = selectedMarker,
        let iconView = selectedMarker.iconView{
            iconView.setValue(UIImage(named: cleaning.numberOfPeople<100 ? "place-small":"place-big"), forKeyPath: "pinImageView.image")
        }
        if let iconView = marker.iconView as? PlacePinView {
            iconView.setValue(UIImage(named: cleaning.numberOfPeople<100 ? "place-small-selected":"place-big-selected"), forKeyPath: "pinImageView.image")
        }
        selectedMarker = marker
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        shortInfoViewHide(true)
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Cleanings.shared.current.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cleanings = Cleanings.shared.current
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShortInfoCollectionViewCell", for: indexPath as IndexPath) as! ShortInfoCollectionViewCell
        cell.point(cleanings[indexPath.row])
        return cell
    }
    
    //MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cleanings = Cleanings.shared.current
        let cleaning = cleanings[indexPath.row]
        self.performSegue(withIdentifier: "DetailedCleaningPoint", sender: cleaning)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let marker = markers[indexPath.row]
        mapView.selectedMarker = marker
        let update = GMSCameraUpdate.setCamera(GMSCameraPosition.camera(withTarget: marker.position, zoom: defaultZoom))
        mapView.animate(with: update)
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: shortInfoCollectionView.frame.size.width - 20, height: shortInfoCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 10, 5, 10)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailedCleaningPoint"
        {
            if let destinationVC = segue.destination as? DetailedCleaningViewController {
                destinationVC.cleaning = sender as! Cleaning!
            }
        }
        segue.destination.az_modalTransition = Transition()
    }
    
    //MARK: - support functions
    
    func showCleningsMap() {
        
        mapView.clear()
        
        for cleaning in Cleanings.shared.current {
            let marker = GMSMarker()
            marker.position =  cleaning.location!
            marker.title = cleaning.title
            marker.snippet = cleaning.address
            marker.iconView = loadPinFromNib(with: cleaning.numberOfPeople)
            marker.userData = cleaning
            marker.appearAnimation = kGMSMarkerAnimationPop
            marker.zIndex = 0
            marker.map = mapView
            markers.append(marker)
        }
        shortInfoViewHide(true)
    }
    
    @IBAction func searchWithinBox(_ sender: UIButton) {
        updateMap()
    }
    
    func catchSearchResult(notification:Notification) -> Void {
        guard let userInfo = notification.userInfo,
            let classType = userInfo["classType"] as? AnyClass,
            classType == self.classForCoder,
            let place = userInfo["place"] as? GMSPlace else {
                return
        }
        mapView.camera = GMSCameraPosition.camera(withTarget: place.coordinate, zoom: defaultZoom)
        let searchMarker = GMSMarker()
        searchMarker.position =  place.coordinate
        searchMarker.title = place.name
        searchMarker.snippet = place.formattedAddress
        searchMarker.userData = place
        searchMarker.appearAnimation = kGMSMarkerAnimationPop
        searchMarker.map = mapView
        mapView.selectedMarker = searchMarker
    }
    
    func shortInfoViewHide(_ isHidden:Bool) {
        UIView.transition(with: shortInfoView, duration: 1, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            self.shortInfoView.isHidden = isHidden
        }, completion: nil)
        UIView.animate(withDuration: 1) {
            self.mapView.padding = isHidden ? UIEdgeInsets() : UIEdgeInsetsMake(100, 0, 100, 0)
            self.shortInfoView.isHidden = isHidden
        }
    }
    
    func loadPinFromNib(with number:Int) -> UIView {
        var nibNamed = "CleaningPinSmall"
        if number > 99 {
            nibNamed = "CleaningPinBig"
        }
        let pinView = UIView.loadViewFromNib(name: nibNamed)
        pinView.setValue(number.description, forKeyPath: "coreLabel.text")
        return pinView
    }

}

extension UIView {
    class func loadViewFromNib(name:String) -> UIView {
        let nib = UINib(nibName: name, bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
