//
//  ShortInfoViewCell.swift
//  LetsDoItUkraine
//
//  Created by Viacheslav Marikutsa on 14.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import GoogleMaps

class ShortInfoCollectionViewCell: UICollectionViewCell {
    
    typealias MapPoint = Any?
    
    @IBOutlet weak internal var icon: UIImageView!
    @IBOutlet weak internal var heading: UILabel!
    @IBOutlet weak internal var subHeading: UILabel!
    @IBOutlet weak internal var designation: UILabel!
    @IBOutlet weak internal var subtitle: UILabel!
    @IBOutlet weak internal var distance: UILabel!
    
    func point<MapPoint: MapPointProtocol> (_ point:MapPoint?) {
        guard let point = point else {
            return
        }
        if let iconUrl = point.icon {
            icon.sd_setImage(with: iconUrl)
        }
        heading.text = point.heading
        subHeading.text = point.subHeading
        designation.text = point.designation
        subtitle.text = point.subtitle
        if let location = point.position,
            let lastLocation = CLLocationManager().location?.coordinate {
            let pointB = GMSMapPoint(x: location.latitude, y: location.longitude)
            let pointA = GMSMapPoint(x: lastLocation.latitude, y: lastLocation.longitude)
            let pointDistance = GMSMapPointDistance(pointA, pointB)
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.maximumFractionDigits = 2
            formatter.roundingMode = .up
            distance.text = formatter.string(from: NSNumber(value: pointDistance))! + " km"
        } else {
            distance.text = "~ km"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

}
