//
//  CleaningCreationViewController.swift
//  LetsDoItUkraine
//
//  Created by Artem Demchenko on 25.10.16.
//  Copyright © 2016 artdmk. All rights reserved.
//

import UIKit
import GooglePlaces
import PKHUD

enum mediaSourceType: Int, CustomStringConvertible {
    case camera = 1, photoLibrary
    var description: String {
        switch self {
        case .camera:
            return "Camera"
        case .photoLibrary:
            return "Gallery"
        }
    }
    
}

class CleaningCreationViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    enum PictureNum: Int {
        case picture1 = 1, picture2, picture3
    }
    
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var aboutTextField: UITextField!
    @IBOutlet weak var addressSnippetImageView: UIImageView!
    @IBOutlet weak var dateTimeSnippetImageView: UIImageView!
    @IBOutlet weak var descriptionSnippetImageView: UIImageView!
    @IBOutlet weak var clearPicture1Button: UIButton!
    @IBOutlet weak var clearPicture2Button: UIButton!
    @IBOutlet weak var clearPicture3Button: UIButton!
    
    @IBOutlet weak var picture1View: UIImageView!
    @IBOutlet weak var picture2View: UIImageView!
    @IBOutlet weak var picture3View: UIImageView!
    var pictures:[PictureNum:UIImage?] = [.picture1:nil,
                                                    .picture2:nil,
                                                    .picture3:nil]
    
    var editingImageKey:PictureNum?
    var dateTime:Date?
    var cleaningLocation:CLLocationCoordinate2D?
    
    var cleaning  = Cleaning(id: nil, title: nil, location: nil, datetime: nil, address: nil, pictures: nil, about: nil, coordinator: nil, numberOfPeople:0)
    var placesClient: GMSPlacesClient?
    var autocompleteTableView:UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .default
        
        if User.current == nil {
            navigationController?.pushViewController(registrationProfileViewController!, animated: true)
        }
        
        addressTextField.delegate = self
        placesClient = GMSPlacesClient()
        
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.clipsToBounds = true
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        dateTextField.inputView = datePicker
        
        picture1View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pickFirstImageActionSheet)))
        picture2View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pickSecondImageActionSheet)))
        picture3View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pickThirdImageActionSheet)))
        
        NotificationCenter.default.addObserver(forName:searchResultNotification,
                                               object:nil, queue:nil,
                                               using:catchSearchResult)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        guard let cleaningLocation = cleaningLocation,
        let dateTime = dateTime else {
            let warningAllert = UIAlertController(title: "Ошибка", message: "Заполните необходимые поля", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            warningAllert.addAction(okAction)
            self.present(warningAllert, animated:true, completion:nil)
            return
        }
        let dict = ["title":addressTextField.text!,
                    "address":addressTextField.text!,
                    "location":cleaningLocation,
                    "datetime":dateTime,
                    "about":aboutTextField.text!,
                    "picture1":pictures[.picture1]!,
                    "picture2":pictures[.picture2]!,
                    "picture3":pictures[.picture3]!] as [String:Any?]
        HUD.show(.progress)
        DataBaseService.shared.createCleaning(dict, callback: { [weak self] (success) in
            if success {
            requestViewController?.az_modalTransition = Transition()
            HUD.flash(.success, delay: 1.0) { finished in
                self?.present(requestViewController!, animated: true, completion: nil)
            }
            } else {
                HUD.flash(.labeledError(title: "Ошибка создания", subtitle: "Проверьте подключение к интернету"))
            }
        })
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self as GMSAutocompleteViewControllerDelegate
        self.present(acController, animated: true, completion: nil)
        return false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == addressTextField {
            addressSnippetImageView.image = addressSnippetImageView.image?.withRenderingMode(.alwaysOriginal)
            cleaningLocation = nil
        }
        return true
    }
    
    //MARK: - supply functions
    
    func placeAutocomplete(notification: Notification) {
        guard let queryString = addressTextField.text,
        queryString != "" else {
            return
        }
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.address
        placesClient?.autocompleteQuery(queryString, bounds: nil, filter: filter, callback: { (results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            guard let results = results as [GMSAutocompletePrediction]! else {
                return
            }
            for result in results as [GMSAutocompletePrediction]! {
                print("Result \(result.attributedPrimaryText.string)")
            }
        })
    }
    
    func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy hh:mm"
        dateTextField.text = dateFormatter.string(from: sender.date)
        dateTime = sender.date
    }

    //MARK: - Pictures
    
    func pickImageActionSheet() {
        let pickAlert = UIAlertController(title:"Выберите источник", message: "Прикрепите фото с галлереи или воспользуйтесь камерой", preferredStyle: .actionSheet)
        let cameraAction =  UIAlertAction(title: mediaSourceType.camera.description, style: .default, handler: self.pickImageFrom)
        let galleryAction =  UIAlertAction(title: mediaSourceType.photoLibrary.description, style: .default, handler: self.pickImageFrom)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        pickAlert.addAction(galleryAction)
        pickAlert.addAction(cameraAction)
        pickAlert.addAction(cancelAction)
        self.present(pickAlert, animated:true, completion:nil)
    }
    
    func pickImageFrom(action:UIAlertAction) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = action.title! == mediaSourceType.camera.description ? .camera : .photoLibrary
        imagePickerController.allowsEditing = false
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let picture = info[UIImagePickerControllerOriginalImage] as? UIImage,
            let editingImageKey = editingImageKey {
            pictures[editingImageKey] = picture
            switch editingImageKey {
            case .picture1:
                picture1View.image = picture
            case .picture2:
                picture2View.image = picture
            case .picture3:
                picture3View.image = picture
            }
            if let picture1 = pictures[.picture1] {
                clearPicture1Button.isHidden = picture1 == nil
            }
            if let picture2 = pictures[.picture2] {
                clearPicture2Button.isHidden = picture2 == nil
            }
            if let picture3 = pictures[.picture3] {
                clearPicture3Button.isHidden = picture3 == nil
            }
        }
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: - Gesture Recognizers
    
    func pickFirstImageActionSheet(_ recognizer:UITapGestureRecognizer) {
        editingImageKey = .picture1
        pickImageActionSheet()
    }
    
    func pickSecondImageActionSheet(_ recognizer:UITapGestureRecognizer) {
        editingImageKey = .picture2
        pickImageActionSheet()
    }
    
    func pickThirdImageActionSheet(_ recognizer:UITapGestureRecognizer) {
        editingImageKey = .picture3
        pickImageActionSheet()
    }
    
    //MARK: - Actions
    
    @IBAction func deletePicture1ButtonTapped(_ sender: UIButton) {
        picture1View.image = UIImage(named: "photo-placeholder")
        pictures[.picture1] = nil
        clearPicture1Button.isHidden = true
    }
    
    @IBAction func deletePicture2ButtonTapped(_ sender: UIButton) {
        picture2View.image = UIImage(named: "photo-placeholder")
        pictures[.picture2] = nil
        clearPicture2Button.isHidden = true
    }
    
    @IBAction func deletePicture3ButtonTapped(_ sender: UIButton) {
        picture3View.image = UIImage(named: "photo-placeholder")
        pictures[.picture3] = nil
        clearPicture3Button.isHidden = true
    }
    
    //MARK: Notification handlers
    
    func catchSearchResult(notification:Notification) -> Void {
        guard let userInfo = notification.userInfo,
            let classType = userInfo["classType"] as? AnyClass,
            classType == self.classForCoder,
            let place = userInfo["place"] as? GMSPlace else {
                return
        }
        cleaningLocation = place.coordinate
        addressTextField.text = place.formattedAddress
        addressSnippetImageView.image = addressSnippetImageView.image?.withRenderingMode(.alwaysTemplate)
    }
    
    deinit {
        print("deinit creation controller")
    }
}
